﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Charts
{
    public class ElementExpandos
    {
        public ElementExpandos()
        {
        }

        private int _yValueIndex;
        private int _seriesIndex;
        private Point _tooltipPreferedPoint;

        /// <summary>
        /// The YValueIndex associated with this element
        /// </summary>
        public int YValueIndex
        {
            get { return _yValueIndex; }
            set { _yValueIndex = value; }
        }

        /// <summary>
        /// The SeriesIndex associated with this element
        /// </summary>    
        public int SeriesIndex
        {
            get { return _seriesIndex; }
            set { _seriesIndex = value; }
        }

        /// <summary>
        /// The prefered location of the tooltip for this element
        /// </summary>
        public Point TooltipPreferedPoint
        {
            get { return _tooltipPreferedPoint; }
            set { _tooltipPreferedPoint = value; }
        }
    }
}
