﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Votela.Services.Data;
using System.Configuration;
using System.Linq.Expressions;
using Votela.Services.ServiceResponses;
using System.Data.Linq;

namespace Votela.Services
{
    public class VotelaService : IVotelaService
    {
        public List<Data.Event> GetEvents()
        {
            using (var context = new VotelaDataModelDataContext(ConfigurationManager.ConnectionStrings["Votela"].ConnectionString))
            {
                return context.Events.ToList();
            }
        }

        public SignIntoEventResponse SignIntoEvent(string eventCode, string evaluatorCode, string evaluatorName)
        {
            using (var context = new VotelaDataModelDataContext(ConfigurationManager.ConnectionStrings["Votela"].ConnectionString))
            {
                context.DeferredLoadingEnabled = false;

                var loadOptions = new System.Data.Linq.DataLoadOptions();
                loadOptions.LoadWith<EventSession>(e => e.Votes);
                loadOptions.LoadWith<Vote>(v => v.EventCategoryVoteCategory);
                loadOptions.LoadWith<Vote>(v => v.Evaluator);
                loadOptions.LoadWith<Event>(ev => ev.Evaluators);
                loadOptions.LoadWith<Event>(ev => ev.EventSessions);
                loadOptions.LoadWith<EventSession>(es => es.EventCategory);
                loadOptions.LoadWith<EventCategory>(ec => ec.EventCategoryVoteCategories);

                context.LoadOptions = loadOptions;

                var matchingEvent = (from e in context.Events
                                     where
                                        e.EventCode == eventCode
                                     select e).FirstOrDefault();

                if (matchingEvent != null)
                {
                    evaluatorCode = evaluatorCode.ToUpperInvariant();
                    var matchingEvaluator = matchingEvent.Evaluators.Where(ev => ev.EvaluatorCode.ToUpperInvariant() == evaluatorCode).FirstOrDefault();

                    if (matchingEvaluator != null)
                    {
                        var sessions = matchingEvent.EventSessions.Select(session => new EventSessionWithScore()
                            {
                                IdEventSession = session.IdEventSession,
                                SessionCode = session.SessionCode,
                                SessionName = session.SessionName,
                                SessionOwnerInstitution = session.SessionOwnerInstitution,
                                SessionOwnerName = session.SessionOwnerName,
                                AverageWeightedScore = (double?)session.Votes.Sum(v => (100 * v.Value / v.EventCategoryVoteCategory.FinalScoreWeighting) / session.EventCategory.EventCategoryVoteCategories.Count) / (session.Votes.Select(m => m.Evaluator.EvaluatorCode).Distinct().Count()),
                                MaximumScore = (double)session.EventCategory.EventCategoryVoteCategories.Sum(evc => evc.FinalScoreWeighting)
                            }).ToList();

                        return new SignIntoEventResponse(matchingEvaluator.IdEvaluator, matchingEvent, sessions);
                    }
                }
            }

            return null;
        }

        public List<EventSessionWithScore> GetEventSessions(int idEvent)
        {
            using (var context = new VotelaDataModelDataContext(ConfigurationManager.ConnectionStrings["Votela"].ConnectionString))
            {
                var loadOptions = new DataLoadOptions();
                loadOptions.LoadWith<EventSession>(e => e.Votes);
                loadOptions.LoadWith<Vote>(v => v.EventCategoryVoteCategory);
                loadOptions.LoadWith<EventSession>(es => es.EventCategory);
                loadOptions.LoadWith<EventCategory>(ec => ec.EventCategoryVoteCategories);
                context.LoadOptions = loadOptions;

                var sessions = from session in context.EventSessions
                               where session.IdEvent == idEvent
                               select new EventSessionWithScore()
                                {
                                    IdEventSession = session.IdEventSession,
                                    SessionCode = session.SessionCode,
                                    SessionName = session.SessionName,
                                    SessionOwnerInstitution = session.SessionOwnerInstitution,
                                    SessionOwnerName = session.SessionOwnerName,
                                    AverageWeightedScore = (double?)session.Votes.Sum(v => (100 * v.Value / v.EventCategoryVoteCategory.FinalScoreWeighting) / session.EventCategory.EventCategoryVoteCategories.Count) / (session.Votes.Select(m => m.Evaluator.EvaluatorCode).Distinct().Count()),
                                    MaximumScore = (double)session.EventCategory.EventCategoryVoteCategories.Sum(evc => evc.FinalScoreWeighting)
                                };

                return sessions.ToList();
            }
        }

        public List<Data.EventCategory> GetEventCategories(int idEvent)
        {
            using (var context = new VotelaDataModelDataContext(ConfigurationManager.ConnectionStrings["Votela"].ConnectionString))
            {
                var categories = from category in context.EventCategories
                                 where category.IdEvent == idEvent
                                 select category;

                return categories.ToList();
            }
        }

        public void VoteForSession(int idEventSession, int idEventCategoryVoteCategory, int idEvaluator, decimal value, string comments)
        {
            using (var context = new VotelaDataModelDataContext(ConfigurationManager.ConnectionStrings["Votela"].ConnectionString))
            {
                var previousVote = (from vote in context.Votes
                                    where
                                        vote.IdEvaluator == idEvaluator &&
                                        vote.IdEventCategoryVoteCategory == idEventCategoryVoteCategory &&
                                        vote.IdEventSession == idEventSession
                                    select vote).FirstOrDefault();

                if (previousVote == null)
                {
                    var vote = new Vote()
                    {
                        IdEvaluator = idEvaluator,
                        IdEventCategoryVoteCategory = idEventCategoryVoteCategory,
                        IdEventSession = idEventSession,
                        Value = value,
                        Comments = comments
                    };

                    context.Votes.InsertOnSubmit(vote);
                }
                else
                {
                    previousVote.Comments = comments;
                    previousVote.Value = value;
                }

                context.SubmitChanges();
            }
        }

        public VotingCategoriesForSessionResponse GetVotingCategoriesForSession(int idSession)
        {
            using (var context = new VotelaDataModelDataContext(ConfigurationManager.ConnectionStrings["Votela"].ConnectionString))
            {
                var categories = (from session in context.EventSessions
                                  where session.IdEventSession == idSession
                                  select new
                                  {
                                      Criteria = session.EventCategory.EventCategoryVoteCategories.ToList(),
                                      EventName = session.Event.EventName
                                  }).FirstOrDefault();

                return new VotingCategoriesForSessionResponse()
                {
                    Criteria = categories.Criteria,
                    EventName = categories.EventName
                };
            }
        }

        public GetVotesForSessionResponse GetVotesForSession(int idEventSession)
        {
            using (var context = new VotelaDataModelDataContext(ConfigurationManager.ConnectionStrings["Votela"].ConnectionString))
            {
                var votes = from vote in context.Votes
                            where vote.IdEventSession == idEventSession
                            select new VoteForSession()
                            {
                                Category = vote.EventCategoryVoteCategory.Category,
                                Value = vote.Value ?? 0.0m,
                                Comment = vote.Comments,
                                CategoryWeighting= vote.EventCategoryVoteCategory.FinalScoreWeighting,
                                MaxValue = vote.EventCategoryVoteCategory.MaxValue
                            };

                return new GetVotesForSessionResponse()
                {
                    Votes = votes.ToList()
                };
            }
        }

        public EventCategoriesAndVotesResponse GetEventCategoriesAndVotes(int idEventSession, int idEvaluator)
        {
            using (var context = new VotelaDataModelDataContext(ConfigurationManager.ConnectionStrings["Votela"].ConnectionString))
            {
                var votes = from vote in context.Votes
                            where vote.IdEventSession == idEventSession
                            select new VoteForSession()
                            {
                                Category = vote.EventCategoryVoteCategory.Category,
                                Value = vote.Value ?? 0.0m,
                                Comment = vote.Comments,
                                CategoryWeighting = vote.EventCategoryVoteCategory.FinalScoreWeighting,
                                MaxValue = vote.EventCategoryVoteCategory.MaxValue
                            };

                var userVotes = from vote in context.Votes
                                where vote.IdEventSession == idEventSession &&
                                        vote.IdEvaluator == idEvaluator
                                select new VoteForSession()
                                {
                                    Category = vote.EventCategoryVoteCategory.Category,
                                    Value = vote.Value ?? 0.0m,
                                    Comment = vote.Comments,
                                    CategoryWeighting = vote.EventCategoryVoteCategory.FinalScoreWeighting,
                                    IdEventCategoryVoteCategory = vote.IdEventCategoryVoteCategory,
                                    MaxValue = vote.EventCategoryVoteCategory.MaxValue
                                };

                var categories = (from session in context.EventSessions
                                  where session.IdEventSession == idEventSession
                                  select new
                                  {
                                      Criteria = session.EventCategory.EventCategoryVoteCategories.ToList(),
                                      EventName = session.Event.EventName
                                  }).FirstOrDefault();

                return new EventCategoriesAndVotesResponse()
                {
                    AllVotes = votes.ToList(),
                    UserVotes = userVotes.ToList(),
                    Categories = categories.Criteria
                };
            }
        }
    }
}
