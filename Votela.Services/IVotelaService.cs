﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Votela.Services.Data;
using Votela.Services.ServiceResponses;

namespace Votela.Services
{
    [ServiceContract]
    public interface IVotelaService
    {
        [OperationContract]
        List<Event> GetEvents();

        [OperationContract]
        SignIntoEventResponse SignIntoEvent(string eventCode, string evaluatorCode, string evaluatorName);

        [OperationContract]
        List<EventSessionWithScore> GetEventSessions(int idEvent);

        [OperationContract]
        List<EventCategory> GetEventCategories(int idEvent);

        [OperationContract]
        EventCategoriesAndVotesResponse GetEventCategoriesAndVotes(int idEventSession, int idEvaluator);

        [OperationContract]
        VotingCategoriesForSessionResponse GetVotingCategoriesForSession(int idSession);

        [OperationContract]
        void VoteForSession(int idEventSession, int idEventCategoryVoteCategory, int idEvaluator, decimal value, string comments);

        [OperationContract]
        GetVotesForSessionResponse GetVotesForSession(int idEventSession);
    }
}
