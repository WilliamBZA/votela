﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Votela.Services.Data;

namespace Votela.Services.ServiceResponses
{
    [DataContract]
    public class EventCategoriesAndVotesResponse
    {
        [DataMember]
        public List<EventCategoryVoteCategory> Categories { get; set; }

        [DataMember]
        public List<VoteForSession> AllVotes { get; set; }

        [DataMember]
        public List<VoteForSession> UserVotes { get; set; }
    }
}
