﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Votela.Services.ServiceResponses
{
    [DataContract]
    public class EventSessionWithScore
    {
        [DataMember]
        public int IdEventSession { get; set; }

        [DataMember]
        public string SessionCode { get; set; }

        [DataMember]
        public string SessionOwnerInstitution { get; set; }

        [DataMember]
        public string SessionName { get; set; }

        [DataMember]
        public string SessionOwnerName { get; set; }

        [DataMember]
        public double? AverageWeightedScore { get; set; }

        [DataMember]
        public double MaximumScore { get; set; }
    }
}