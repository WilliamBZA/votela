﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Votela.Services.ServiceResponses
{
    [DataContract]
    public class GetVotesForSessionResponse
    {
        [DataMember]
        public List<VoteForSession> Votes { get; set; }
    }
}