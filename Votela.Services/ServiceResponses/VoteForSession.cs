﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Votela.Services.ServiceResponses
{
    [DataContract]
    public class VoteForSession
    {
        [DataMember]
        public string Category { get; set; }

        [DataMember]
        public decimal Value { get; set; }

        [DataMember]
        public decimal MaxValue { get; set; }

        [DataMember]
        public decimal CategoryWeighting { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public int IdEventCategoryVoteCategory { get; set; }
    }
}