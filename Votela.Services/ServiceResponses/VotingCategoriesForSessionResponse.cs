﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Votela.Services.Data;

namespace Votela.Services.ServiceResponses
{
    [DataContract]
    public class VotingCategoriesForSessionResponse
    {
        [DataMember]
        public string EventName { get; set; }

        [DataMember]
        public List<EventCategoryVoteCategory> Criteria { get; set; }
    }
}