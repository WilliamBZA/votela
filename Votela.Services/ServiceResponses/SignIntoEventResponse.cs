﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Votela.Services.Data;
using System.Runtime.Serialization;

namespace Votela.Services.ServiceResponses
{
    [DataContract]
    public class SignIntoEventResponse
    {
        public SignIntoEventResponse(int idEvaluator, Event @event, List<EventSessionWithScore> sessions)
        {
            IdEvaluator = idEvaluator;
            Event = @event;
            Sessions = sessions;
        }

        [DataMember]
        public int IdEvaluator { get; set; }

        [DataMember]
        public Event Event { get; set; }

        [DataMember]
        public List<EventSessionWithScore> Sessions { get; set; }
    }
}