﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Charts;

namespace Votela.Client.WP7
{
    public partial class EventSessions : PhoneApplicationPage
    {
        private VotelaService.VotelaServiceClient _client;

        public EventSessions()
        {
            InitializeComponent();
        }

        private VotelaService.VotelaServiceClient Client
        {
            get
            {
                if (_client != null)
                {
                    if ((_client.State == System.ServiceModel.CommunicationState.Faulted || _client.State == System.ServiceModel.CommunicationState.Closed))
                    {
                        _client = null;
                    }
                }

                if (_client == null)
                {
#if DEBUG
                    _client = new VotelaService.VotelaServiceClient("BasicHttpBinding_IVotelaService", "http://localhost:44450/votela.svc");    
#else
                    _client = new VotelaService.VotelaServiceClient();
#endif
                }

                return _client;
            }
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = (App.Current as App).Event;

            double maxSessionScore = (App.Current as App).Event.Sessions.Max(s => s.MaximumScore);

            LoadRatingsGraph((App.Current as App).Event.Sessions, maxSessionScore);

            SetLoading(false);
        }

        private void LoadRatingsGraph(List<ClientModel.Session> list, double maxValue)
        {
            Chart container = RatingsChart;
            ChartModel model = LoadChartModel(list, maxValue);

            if (container.ActualWidth > 0)
            {
                // create the chart 
                Chart chart = Charts.Chart.CreateChart(Charts.Chart.ChartType.VBAR, model);

                //set the chart to be in 2D or in 3D
                chart.IsPerspective = false;
                chart.Background = container.Background;

                //set the position of the series labels
                chart.LegendPosition = Chart.LegendLocation.END;

                //set the duration of the animation
                chart.AnimationDuration = 0;

                //set the formate of the 
                chart.Format = "#0.0";

                //set the size of the chart area.
                chart.SetBounds(new Rect(0, 0, container.ActualWidth, container.ActualHeight));
                // add the custom component
                container.Children.Clear();
                container.Children.Add(chart);

                // now draw it
                chart.Draw();
            }
        }

        private T[,] JaggedToMultidimensional<T>(IEnumerable<T[]> jaggedArray)
        {
            int rows = jaggedArray.Count();
            if (rows > 0)
            {
                int cols = jaggedArray.Max(subArray => subArray.Length);
                T[,] array = new T[cols, rows];
                for (int i = 0; i < rows; i++)
                {
                    for (int j = 0; j < cols; j++)
                    {
                        array[j, i] = jaggedArray.ElementAt(i).ElementAt(j);//[i][j];
                    }
                }

                return array;
            }

            return new T[0, 0];
        }

        private ChartModel LoadChartModel(IEnumerable<ClientModel.Session> sessions, double maxValue)
        {
            var top10 = (from session in sessions
                         orderby session.AverageWeightedScore descending
                         select session).Take(10);

            // the name of each series.
            string[] _seriesLabels = top10.Select(s => s.SessionName).ToArray();

            string[] _seriesColors = { "#E76D48", "#6EA6F3", "#9DCE6E", "#FCC46F", "#FF7FFF", "#D06DFF", "#A3FFEB" };

            var values = JaggedToMultidimensional<double>(top10.Select(s => new double[] { s.AverageWeightedScore }));


            Color[] colors = new Color[_seriesColors.Length + 2];
            colors[0] = (Color)Application.Current.Resources["PhoneAccentColor"];
            colors[1] = (Color)Application.Current.Resources["PhoneForegroundColor"];
            for (var i = 0; i < _seriesColors.Length; ++i)
            {
                byte r = byte.Parse(_seriesColors[i].Substring(1, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                byte g = byte.Parse(_seriesColors[i].Substring(3, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                byte b = byte.Parse(_seriesColors[i].Substring(5, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                colors[i + 2] = Color.FromArgb(255, r, g, b);
            }

            double minYValue = 0;
            double maxYValue = maxValue;

            string title = string.Empty;
            string subTitle = string.Empty;

            ChartModel model = new ChartModel(_seriesLabels, new string[] { string.Empty }, values, null, title, subTitle, null, colors);

            model.MaxYValue = maxYValue;
            model.MinYValue = minYValue;

            return model;
        }

        private void SessionButton_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;

            NavigationService.Navigate(new Uri(string.Format("/EventSessionDetails.xaml?IdEventSession={0}", button.Tag), UriKind.Relative));
        }

        private void SetLoading(bool busyLoading)
        {
            if (busyLoading)
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Loading.Visibility = System.Windows.Visibility.Visible;
                    Sessions.Visibility = System.Windows.Visibility.Collapsed;
                });
            }
            else
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Sessions.Visibility = System.Windows.Visibility.Visible;
                    Loading.Visibility = System.Windows.Visibility.Collapsed;
                });
            }
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            SetLoading(true);

            try
            {
                Client.GetEventSessionsCompleted += Client_GetEventSessionsCompleted;
                Client.GetEventSessionsAsync((App.Current as App).IdEvent);
            }
            catch
            {
                Dispatcher.BeginInvoke(() => MessageBox.Show("Looks like something went wrong. Give it another try or check your network connectivity."));
            }
        }

        void Client_GetEventSessionsCompleted(object sender, VotelaService.GetEventSessionsCompletedEventArgs e)
        {
            Client.GetEventSessionsCompleted -= Client_GetEventSessionsCompleted;

            var @event = (App.Current as App).Event;

            @event.Sessions = e.Result.Select(s => new ClientModel.Session
                                                                {
                                                                    IdSession = s.IdEventSession,
                                                                    SessionCode = s.SessionCode,
                                                                    SessionInstitutionName = s.SessionOwnerInstitution,
                                                                    SessionName = s.SessionName,
                                                                    SessionOwnerName = s.SessionOwnerName,
                                                                    AverageWeightedScore = s.AverageWeightedScore ?? 0,
                                                                    MaximumScore = s.MaximumScore
                                                                }).ToList();

            (App.Current as App).Event = @event;
            DataContext = (App.Current as App).Event;

            var maximumScore = (double)@event.Sessions.Max(s => s.MaximumScore);

            LoadRatingsGraph((App.Current as App).Event.Sessions, maximumScore);

            SetLoading(false);
        }

        private void PhoneApplicationPage_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Client.CloseAsync();
            }
            catch { }
            
            _client = null;
        }

        private void Sessions_LoadedPivotItem(object sender, PivotItemEventArgs e)
        {
            if (e.Item.Header as string == "Ratings")
            {
                var maximumScore = (App.Current as App).Event.Sessions.Max(s => s.MaximumScore);

                LoadRatingsGraph((App.Current as App).Event.Sessions, maximumScore);
            }
        }

        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Votela.About;component/About.xaml", UriKind.Relative));
        }
    }
}