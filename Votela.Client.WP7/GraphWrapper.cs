﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Votela.Client.WP7
{
    public class GraphWrapper
    {
        public string Category { get; set; }

        public double Value { get; set; }
    }
}