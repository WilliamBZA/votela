﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Charts;
using System.Threading.Tasks;

namespace Votela.Client.WP7
{
    public partial class MainPage : PhoneApplicationPage
    {
        private VotelaService.VotelaServiceClient _client;
        
        public MainPage()
        {
            InitializeComponent();
        }

        private void Logon_Click(object sender, RoutedEventArgs e)
        {
            SetLoading(true);

            var eventCode = EventCode.Text;
            var evaluatorCode = EvaluatorCode.Text;

            new TaskFactory().StartNew(() =>
                {
                    try
                    {
                        Client.SignIntoEventCompleted += client_SignIntoEventCompleted;
                        Client.SignIntoEventAsync(eventCode, evaluatorCode, null);
                    }
                    catch (Exception)
                    {
                        Dispatcher.BeginInvoke(() => MessageBox.Show("Looks like something went wrong. Give it another try or check your network connectivity."));
                    }
                });
        }

        private VotelaService.VotelaServiceClient Client
        {
            get
            {
                if (_client != null)
                {
                    if ((_client.State == System.ServiceModel.CommunicationState.Faulted || _client.State == System.ServiceModel.CommunicationState.Closed))
                    {
                        _client = null;
                    }
                }

                if (_client == null)
                {
#if DEBUG
                    _client = new VotelaService.VotelaServiceClient("BasicHttpBinding_IVotelaService", "http://localhost:44450/votela.svc");    
#else
                    _client = new VotelaService.VotelaServiceClient();
#endif
                }

                return _client;
            }
        }

        protected void client_SignIntoEventCompleted(object sender, VotelaService.SignIntoEventCompletedEventArgs e)
        {
            Client.SignIntoEventCompleted -= client_SignIntoEventCompleted;

            if (e.Result == null)
            {
                Dispatcher.BeginInvoke(() => 
                    {
                        MessageBox.Show("Looks like you provided to wrong details. Double-check the event code and your evaluator code and try again.");
                        SetLoading(false);
                    });
            }
            else
            {
                (App.Current as App).IdEvaluator = e.Result.IdEvaluator;
                (App.Current as App).IdEvent = e.Result.Event.IdEvent;

                (App.Current as App).Event = new ClientModel.Event
                {
                    EventName = e.Result.Event.EventName,
                    Sessions = e.Result.Sessions.Select(s => new ClientModel.Session 
                                                                {
                                                                    IdSession = s.IdEventSession,
                                                                    SessionCode = s.SessionCode,
                                                                    SessionInstitutionName = s.SessionOwnerInstitution,
                                                                    SessionName = s.SessionName,
                                                                    SessionOwnerName = s.SessionOwnerName,
                                                                    AverageWeightedScore = s.AverageWeightedScore ?? 0,
                                                                    MaximumScore = s.MaximumScore
                                                                }).ToList()
                };

                Dispatcher.BeginInvoke(() => 
                    {
                        NavigationService.Navigate(new Uri("/EventSessions.xaml", UriKind.Relative));
                        //SetLoading(false);
                    });
            }
        }

        private void PhoneApplicationPage_Unloaded(object sender, RoutedEventArgs e)
        {
            SetLoading(false);

            try
            {
                Client.CloseAsync();
            }
            catch { }

            _client = null;
        }

        public void SetLoading(bool busyLoading)
        {
            if (busyLoading)
            {
                Loading.Visibility = System.Windows.Visibility.Visible;
                ContentPanel.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                ContentPanel.Visibility = System.Windows.Visibility.Visible;
                Loading.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Votela.About;component/About.xaml", UriKind.Relative));
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            SetLoading(false);
        }
    }
}