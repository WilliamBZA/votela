﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Votela.Client.WP7.ClientModel;
using System.IO;
using System.Windows.Markup;
using Charts;
using System.Collections.ObjectModel;
using Votela.Client.WP7.VotelaService;
using System.Threading.Tasks;

namespace Votela.Client.WP7
{
    public partial class EventSessionDetails : PhoneApplicationPage
    {
        private VotelaService.VotelaServiceClient _client;
        private EventSessionDetail _sessionCriteria;
        private int _idEventSession;
        private System.Collections.ObjectModel.ObservableCollection<VotelaService.VoteForSession> _voteData;

        public EventSessionDetails()
        {
            InitializeComponent();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            int idEventSession;
            int.TryParse(NavigationContext.QueryString["IdEventSession"] as string, out idEventSession);

            LoadDetails(idEventSession);
        }

        private void LoadDetails(int idEventSession)
        {
            SetLoading(true);

            _idEventSession = idEventSession;

            try
            {
                Client.GetEventCategoriesAndVotesCompleted += Client_GetEventCategoriesAndVotesCompleted;
                Client.GetEventCategoriesAndVotesAsync(_idEventSession, (App.Current as App).IdEvaluator);
            }
            catch
            {
                Dispatcher.BeginInvoke(() => MessageBox.Show("Looks like something went wrong. Give it another try or check your network connectivity."));
            }
        }

        private void LoadJustVotes(int idEventSession)
        {
            SetLoading(true);

            try
            {
                Client.GetEventCategoriesAndVotesCompleted += Client_GetEventCategoriesAndVotesCompleted_JustVotes;
                Client.GetEventCategoriesAndVotesAsync(_idEventSession, (App.Current as App).IdEvaluator);
            }
            catch
            {
                Dispatcher.BeginInvoke(() => MessageBox.Show("Looks like something went wrong. Give it another try or check your network connectivity."));
            }
        }

        void Client_GetEventCategoriesAndVotesCompleted(object sender, VotelaService.GetEventCategoriesAndVotesCompletedEventArgs e)
        {
            Client.GetEventCategoriesAndVotesCompleted -= Client_GetEventCategoriesAndVotesCompleted;

            AddVoteCategories(e.Result.Categories, e.Result.UserVotes);

            LoadJustVotes(_idEventSession);
        }

        void Client_GetEventCategoriesAndVotesCompleted_JustVotes(object sender, VotelaService.GetEventCategoriesAndVotesCompletedEventArgs e)
        {
            Client.GetEventCategoriesAndVotesCompleted -= Client_GetEventCategoriesAndVotesCompleted_JustVotes;

            LoadVotes(e.Result.AllVotes, e.Result.UserVotes);

            SetLoading(false);
        }

        private void LoadVotes(System.Collections.ObjectModel.ObservableCollection<VotelaService.VoteForSession> allVotes, System.Collections.ObjectModel.ObservableCollection<VotelaService.VoteForSession> userVotes)
        {
            _voteData = allVotes;
        }

        private void AddVoteCategories(System.Collections.ObjectModel.ObservableCollection<VotelaService.VoteForSession> allVotes, System.Collections.ObjectModel.ObservableCollection<VotelaService.VoteForSession> userVotes)
        {
            _sessionCriteria = new EventSessionDetail()
            {
                EventName = (App.Current as App).Event.EventName,
                VotingCriteria = userVotes.Select(c => new SessionCategoryVote() { Category = c.Category, Value = c.Value, IdEventCategoryVoteCategory = c.IdEventCategoryVoteCategory, MaxValue = c.MaxValue }).ToList()
            };

            DataContext = _sessionCriteria;

            _voteData = allVotes;
        }

        private void AddVoteCategories(IEnumerable<EventCategoryVoteCategory> categories, System.Collections.ObjectModel.ObservableCollection<VotelaService.VoteForSession> userVotes)
        {
            _sessionCriteria = new EventSessionDetail()
            {
                EventName = (App.Current as App).Event.EventName,
                VotingCriteria = categories.Select(c => new SessionCategoryVote() { Category = c.Category, Value = userVotes.Where(uv => uv.Category == c.Category).Select(uv => uv.Value).FirstOrDefault(), IdEventCategoryVoteCategory = c.IdEventCategoryVoteCategory, MaxValue = c.MaxValue }).ToList()
            };

            DataContext = _sessionCriteria;

            _voteData = null;
        }

        private VotelaService.VotelaServiceClient Client
        {
            get
            {
                if (_client == null)
                {
#if DEBUG
                    _client = new VotelaService.VotelaServiceClient("BasicHttpBinding_IVotelaService", "http://localhost:44450/votela.svc");
#else
                    _client = new VotelaService.VotelaServiceClient();
#endif
                }

                return _client;
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (var item in Criteria.Items)
                {
                    var vote = item as SessionCategoryVote;

                    if (vote != null)
                    {
                        Client.VoteForSessionAsync(_idEventSession, vote.IdEventCategoryVoteCategory, (App.Current as App).IdEvaluator, vote.Value, null);
                    }
                }

                Dispatcher.BeginInvoke(() => MessageBox.Show("Scores Saved!"));
            }
            catch
            {
                Dispatcher.BeginInvoke(() => MessageBox.Show("Looks like something went wrong. Give it another try or check your network connectivity."));
            }
        }

        private void SetLoading(bool busyLoading)
        {
            if (busyLoading)
            {
                Dispatcher.BeginInvoke(() =>
                    {
                        Loading.Visibility = System.Windows.Visibility.Visible;
                        SessionRatings.Visibility = System.Windows.Visibility.Collapsed;
                    });
            }
            else
            {
                Dispatcher.BeginInvoke(() =>
                {
                    SessionRatings.Visibility = System.Windows.Visibility.Visible;
                    Loading.Visibility = System.Windows.Visibility.Collapsed;
                });
            }
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            LoadJustVotes(_idEventSession);
        }

        private void PhoneApplicationPage_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Client.CloseAsync();
            }
            catch { }

            _client = null;
        }

        private void Charts_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri(string.Format("/EventSessionDetailsCharts.xaml?IdEventSession={0}", _idEventSession), UriKind.Relative));
        }

        private void Back_Click(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Votela.About;component/About.xaml", UriKind.Relative));
        }
    }
}