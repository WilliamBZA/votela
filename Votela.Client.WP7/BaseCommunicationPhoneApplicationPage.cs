﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace Votela.Client.WP7
{
    public class BaseCommunicationPhoneApplicationPage : PhoneApplicationPage
    {
        private VotelaService.VotelaServiceClient _client;

        private VotelaService.VotelaServiceClient Client
        {
            get
            {
                if (_client != null)
                {
                    if ((_client.State == System.ServiceModel.CommunicationState.Faulted || _client.State == System.ServiceModel.CommunicationState.Closed))
                    {
                        _client = null;
                    }
                }

                if (_client == null)
                {
#if DEBUG
                    _client = new VotelaService.VotelaServiceClient("BasicHttpBinding_IVotelaService", "http://localhost:44450/votela.svc");    
#else
                    _client = new VotelaService.VotelaServiceClient();
#endif
                }

                return _client;
            }
        }
    }
}