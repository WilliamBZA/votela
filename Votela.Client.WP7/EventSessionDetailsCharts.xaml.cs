﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Charts;
using System.Collections.ObjectModel;
using Votela.Client.WP7.ClientModel;
using Votela.Client.WP7.VotelaService;

namespace Votela.Client.WP7
{
    public partial class EventSessionDetailsCharts : PhoneApplicationPage
    {
        private VotelaService.VotelaServiceClient _client;
        private int _idEventSession;
        private EventSessionDetail _sessionCriteria;
        private ObservableCollection<VotelaService.VoteForSession> _voteData;
        private bool _alreadyLoaded;

        public EventSessionDetailsCharts()
        {
            InitializeComponent();
        }

        private VotelaService.VotelaServiceClient Client
        {
            get
            {
                if (_client == null)
                {
#if DEBUG
                    _client = new VotelaService.VotelaServiceClient("BasicHttpBinding_IVotelaService", "http://localhost:44450/votela.svc");    
#else
                    _client = new VotelaService.VotelaServiceClient();
#endif
                }

                return _client;
            }
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            _alreadyLoaded = false;
            int idEventSession;
            int.TryParse(NavigationContext.QueryString["IdEventSession"] as string, out idEventSession);

            LoadDetails(idEventSession);
        }

        private void LoadDetails(int idEventSession)
        {
            SetLoading(true);

            _idEventSession = idEventSession;

            try
            {
                Client.GetEventCategoriesAndVotesCompleted += Client_GetEventCategoriesAndVotesCompleted_JustVotes;
                Client.GetEventCategoriesAndVotesAsync(_idEventSession, (App.Current as App).IdEvaluator);
            }
            catch
            {
                Dispatcher.BeginInvoke(() => MessageBox.Show("Looks like something went wrong. Give it another try or check your network connectivity."));
            }
        }

        private void AddVoteCategories(IEnumerable<EventCategoryVoteCategory> categories, System.Collections.ObjectModel.ObservableCollection<VotelaService.VoteForSession> allVotes)
        {
            _sessionCriteria = new EventSessionDetail()
            {
                EventName = (App.Current as App).Event.EventName,
                VotingCriteria = categories.Select(c => new SessionCategoryVote() { Category = c.Category, Value = allVotes.Where(uv => uv.Category == c.Category).Select(uv => uv.Value).FirstOrDefault(), IdEventCategoryVoteCategory = c.IdEventCategoryVoteCategory, MaxValue = c.MaxValue }).ToList()
            };

            DataContext = _sessionCriteria;

            _voteData = allVotes;
        }

        void Client_GetEventCategoriesAndVotesCompleted_JustVotes(object sender, VotelaService.GetEventCategoriesAndVotesCompletedEventArgs e)
        {
            Client.GetEventCategoriesAndVotesCompleted -= Client_GetEventCategoriesAndVotesCompleted_JustVotes;

            AddVoteCategories(e.Result.Categories, e.Result.AllVotes);

            LoadCriteria(e.Result.Categories);

            CreateCharts(e.Result.AllVotes);

            SetLoading(false);
        }

        private void LoadOverallGraph(Chart chart, ObservableCollection<VotelaService.VoteForSession> allVotes)
        {
            if (chart.ActualHeight > 0)
            {
                var chartModel = LoadChartModel(
                    (from v in allVotes group v by v.Category into grouping select grouping.Key).ToArray(), new string[] { string.Empty },
                    JaggedToMultidimensional<double>((from v in allVotes group v by v.Category into grouping select new double[] { (double)grouping.Average(v => v.Value) }).ToArray()),
                    (double?)allVotes.Max(v => v.MaxValue) ?? 10);

                LoadOverallChart(chart, chartModel);
            }
        }

        private void LoadOverallChart(Chart container, ChartModel model)
        {
            // create the chart 
            Chart chart = Charts.Chart.CreateChart(Charts.Chart.ChartType.RADAR_AREA, model);

            //set the chart to be in 2D or in 3D
            chart.IsPerspective = false;
            chart.Background = container.Background;

            //set the position of the series labels
            chart.LegendPosition = Chart.LegendLocation.NONE;

            //set the duration of the animation
            chart.AnimationDuration = 0.6;

            //set the formate of the 
            chart.Format = "#0.0";

            //set the size of the chart area.
            chart.SetBounds(new Rect(0, 0, container.ActualWidth, container.ActualHeight));
            // add the custom component
            container.Children.Clear();
            container.Children.Add(chart);

            // setup the event handler so that when mouse enter show show the information of the bar. 
            //chart.ChartClicked += new ChartEventHandler(ChartClicked);

            // now draw it
            chart.Draw();
        }

        private T[,] JaggedToMultidimensional<T>(IEnumerable<T[]> jaggedArray)
        {
            int rows = jaggedArray.Count();
            if (rows > 0)
            {
                int cols = jaggedArray.Max(subArray => subArray.Length);
                T[,] array = new T[rows, cols];
                for (int i = 0; i < rows; i++)
                {
                    for (int j = 0; j < cols; j++)
                    {
                        array[i, j] = jaggedArray.ElementAt(i).ElementAt(j);//[i][j];
                    }
                }

                return array;
            }

            return new T[0, 0];
        }

        private T[,] JaggedToInvertedMultidimensional<T>(IEnumerable<T[]> jaggedArray)
        {
            int rows = jaggedArray.Count();
            if (rows > 0)
            {
                int cols = jaggedArray.Max(subArray => subArray.Length);
                T[,] array = new T[cols, rows];
                for (int i = 0; i < rows; i++)
                {
                    for (int j = 0; j < cols; j++)
                    {
                        array[j, i] = jaggedArray.ElementAt(i).ElementAt(j);//[i][j];
                    }
                }

                return array;
            }

            return new T[0, 0];
        }

        private ChartModel LoadChartModel(IEnumerable<string> groupLabels, IEnumerable<string> seriesLabels, double[,] chartYValues, double maxValue)
        {
            string[] _seriesColors = { "#E76D48", "#6EA6F3", "#9DCE6E", "#FCC46F", "#FF7FFF" };

            Color[] colors = new Color[_seriesColors.Length + 2];
            colors[0] = (Color)Application.Current.Resources["PhoneAccentColor"];
            colors[1] = (Color)Application.Current.Resources["PhoneForegroundColor"];
            for (var i = 0; i < _seriesColors.Length; ++i)
            {
                byte r = byte.Parse(_seriesColors[i].Substring(1, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                byte g = byte.Parse(_seriesColors[i].Substring(3, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                byte b = byte.Parse(_seriesColors[i].Substring(5, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                colors[i + 2] = Color.FromArgb(255, r, g, b);
            }

            double minYValue = 0;
            double maxYValue = maxValue;

            string title = string.Empty;
            string subTitle = string.Empty;

            ChartModel model = new ChartModel(seriesLabels.ToArray(), groupLabels.ToArray(), chartYValues, null, title, subTitle, null, colors);

            model.MaxYValue = maxYValue;
            model.MinYValue = minYValue;

            return model;
        }

        private void CreateCharts(System.Collections.ObjectModel.ObservableCollection<VotelaService.VoteForSession> allVotes)
        {
            LoadOverallGraph(OverallChart, allVotes);

            var categories = _sessionCriteria.VotingCriteria.GroupBy(c => c.Category).Select(c => c.Key);

            foreach (var criteria in categories)
            {
                var pivot = SessionDetailsPivot.Items.Where(p => (p as PivotItem).Header as string == criteria).FirstOrDefault() as PivotItem;

                if (pivot != null)
                {
                    var grid = pivot.Content as Grid;
                    var graph = grid.Children.FirstOrDefault() as Chart;

                    var votes = from v in allVotes where v.Category == criteria select v;
                    var maxValue = (double?)(from v in votes select v.MaxValue).Max() ?? 10;

                    var values = JaggedToInvertedMultidimensional<double>((from v in votes select new double[] { (double)v.Value }));

                    LoadGraph(graph, votes, values, maxValue);
                }
            }
        }

        private void LoadGraph(Chart chart, IEnumerable<VotelaService.VoteForSession> votes, double[,] values, double maxValue)
        {
            if (chart.ActualHeight > 0)
            {
                var chartModel = LoadChartModel(new string[] { string.Empty },
                    Enumerable.Range(1, values.Length).Select(i => string.Empty),
                    values,
                    maxValue);

                LoadLineChart(chart, chartModel);
            }
        }

        protected void LoadCriteria(System.Collections.ObjectModel.ObservableCollection<VotelaService.EventCategoryVoteCategory> criteria)
        {
            if (!_alreadyLoaded)
            {
                _alreadyLoaded = true;
                
                foreach (var criterion in criteria)
                {
                    var pivot = new PivotItem();
                    pivot.Header = criterion.Category;

                    Grid pivotGrid = new Grid();
                    Chart graph = new Chart();
                    graph.Margin = new Thickness(20);

                    pivotGrid.Children.Add(graph);
                    pivot.Content = pivotGrid;

                    SessionDetailsPivot.Items.Add(pivot);
                }
            }
        }

        private void LoadLineChart(Chart container, ChartModel model)
        {
            Chart chart = Charts.Chart.CreateChart(Charts.Chart.ChartType.VBAR, model);

            chart.IsPerspective = false;
            chart.LegendPosition = Chart.LegendLocation.NONE;
            chart.Format = "#0.0";
            chart.AnimationDuration = 0;

            chart.SetBounds(new Rect(0, 0, container.ActualWidth, container.ActualHeight));
            container.Children.Clear();
            container.Children.Add(chart);

            chart.Draw();
        }        

        private void SetLoading(bool busyLoading)
        {
            if (busyLoading)
            {
                Dispatcher.BeginInvoke(() =>
                {
                    Loading.Visibility = System.Windows.Visibility.Visible;
                    SessionDetailsPivot.Visibility = System.Windows.Visibility.Collapsed;
                });
            }
            else
            {
                Dispatcher.BeginInvoke(() =>
                {
                    SessionDetailsPivot.Visibility = System.Windows.Visibility.Visible;
                    Loading.Visibility = System.Windows.Visibility.Collapsed;
                });
            }
        }

        private void SessionDetailsPivot_LoadedPivotItem(object sender, PivotItemEventArgs e)
        {
            string header = e.Item.Header as string;

            if (string.Compare(header, "ratings", StringComparison.InvariantCultureIgnoreCase) != 0)
            {
                // Check if this pivot has been loaded yet
                var chart = GetGraphOnPivot(e.Item);
                var childChart = GetChildChart(chart);

                if (childChart == null || childChart.Model == null)
                {
                    // if not, load it.
                    if (string.Compare(header, "Overall", StringComparison.InvariantCultureIgnoreCase) != 0)
                    {
                        var votes = _voteData.Where(v => v.Category == header);
                        var maxValue = (double?)(from v in votes select v.MaxValue).Max() ?? 10;

                        LoadGraph(chart, _voteData, JaggedToInvertedMultidimensional<double>(votes.Select(v => new double[] { (double)v.Value })), maxValue);
                    }
                    else
                    {
                        LoadOverallGraph(chart, _voteData);
                    }
                }
            }
        }

        private Chart GetChildChart(Chart graph)
        {
            return graph.Children.FirstOrDefault() as Chart;
        }

        private Chart GetGraphOnPivot(PivotItem pivotItem)
        {
            var grid = pivotItem.Content as Grid;
            var graph = grid.Children.FirstOrDefault() as Chart;

            return graph;
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            LoadJustVotes(_idEventSession);
        }

        private void LoadJustVotes(int idEventSession)
        {
            SetLoading(true);

            try
            {
                Client.GetEventCategoriesAndVotesCompleted += Client_GetEventCategoriesAndVotesCompleted_JustVotes;
                Client.GetEventCategoriesAndVotesAsync(_idEventSession, (App.Current as App).IdEvaluator);
            }
            catch
            {
                Dispatcher.BeginInvoke(() => MessageBox.Show("Looks like something went wrong. Give it another try or check your network connectivity."));
            }
        }

        private void Back_Click(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Votela.About;component/About.xaml", UriKind.Relative));
        }

        private void PhoneApplicationPage_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Client.CloseAsync();
            }
            catch { }
            _client = null;
        }
    }
}