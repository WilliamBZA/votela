﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Votela.Client.WP7.ClientModel
{
    public class SessionCategoryVote : DependencyObject
    {
        public int IdEventCategoryVoteCategory { get; set; }

        public string Category { get; set; }

        public decimal MaxValue { get; set; }

        public decimal Value
        {
            get { return (decimal)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, (decimal)Math.Round(value, 1)); }
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(decimal), typeof(SessionCategoryVote), new PropertyMetadata(0.0m));
    }
}