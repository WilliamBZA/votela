﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Votela.Client.WP7.ClientModel
{
    public class Session
    {
        public int IdSession { get; set; }

        public string SessionName { get; set; }

        public string SessionCode { get; set; }

        public string SessionOwnerName { get; set; }

        public string SessionInstitutionName { get; set; }

        public double AverageWeightedScore { get; set; }

        public double MaximumScore { get; set; }

        public string FullSessionName
        {
            get
            {
                return string.Format("{0} - {1}", SessionCode, SessionName, SessionOwnerName, SessionInstitutionName);
            }
        }
    }
}